#!/usr/bin/env bash
set -euxo pipefail

image_names=(

)

for image_name in "${image_names[@]}"; do
    ./build_one.sh "$image_name"
done
