set -euxo pipefail

apt-get update
apt-get upgrade -y
apt-get install -y --no-install-recommends \
  git \
  htop \
  vim \
  rsync \
  dirmngr

# CUDA
wget https://developer.download.nvidia.com/compute/cuda/repos/ubuntu1804/x86_64/7fa2af80.pub
echo "47217c49dcb9e47a8728b354450f694c9898cd4a126173044a69b1e9ac0fba96  7fa2af80.pub" | sha256sum -c
cat 7fa2af80.pub | apt-key add -
rm 7fa2af80.pub

wget https://developer.download.nvidia.com/compute/cuda/repos/ubuntu2004/x86_64/cuda-ubuntu2004.pin
echo "dd00df91301f85f920a43641113793b3e8d6006e058e36fc69f44eadaebf648a  cuda-ubuntu2004.pin" | sha256sum -c
mv cuda-ubuntu2004.pin /etc/apt/preferences.d/cuda-repository-pin-600

add-apt-repository "deb http://developer.download.nvidia.com/compute/cuda/repos/ubuntu2004/x86_64/ /"
apt-get update
apt-get install -y --no-install-recommends \
  cuda-drivers

# Docker
apt-get install -y  --no-install-recommends \
  apt-transport-https \
  ca-certificates \
  curl \
  gnupg \
  gnupg-agent \
  software-properties-common

wget -O docker.gpg https://download.docker.com/linux/ubuntu/gpg
echo "1500c1f56fa9e26b9b8f42452a553675796ade0807cdce11975eb98170b3a570  docker.gpg" | sha256sum -c
cat docker.gpg | apt-key add -
rm docker.gpg

add-apt-repository \
"deb [arch=amd64] https://download.docker.com/linux/ubuntu \
 $(lsb_release -cs) \
 stable"

apt-get install -y --no-install-recommends \
  docker-ce \
  docker-ce-cli \
  containerd.io

# CUDA container toolkit
wget -O nvidia-docker.gpg https://nvidia.github.io/nvidia-docker/gpgkey
echo "c880576d6cf75a48e5027a871bac70fd0421ab07d2b55f30877b21f1c87959c9  nvidia-docker.gpg" | sha256sum -c
cat nvidia-docker.gpg | apt-key add -
rm nvidia-docker.gpg

wget https://nvidia.github.io/nvidia-docker/ubuntu20.04/nvidia-docker.list
echo "1727985494fbd19e3b963880d15117487435cbabef4e295484111f003cf03d41  nvidia-docker.list" | sha256sum -c
mv nvidia-docker.list /etc/apt/sources.list.d/nvidia-docker.list

apt-get update
apt-get install -y --no-install-recommends nvidia-docker2
