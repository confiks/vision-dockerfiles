# Taken from: https://github.com/anibali/docker-pytorch/blob/master/dockerfiles/1.10.0-cuda11.3-ubuntu20.04/Dockerfile

FROM nvidia/cuda:11.3.1-base-ubuntu20.04

# Install some basic utilities
RUN apt-get update && apt-get install -y \
    curl \
    ca-certificates \
    sudo \
    git \
    bzip2 \
    libx11-6 \
 && rm -rf /var/lib/apt/lists/*

# Create a working directory
RUN mkdir /app
WORKDIR /app

# Create a non-root user and switch to it
RUN adduser --disabled-password --gecos '' --shell /bin/bash user \
 && chown -R user:user /app
RUN echo "user ALL=(ALL) NOPASSWD:ALL" > /etc/sudoers.d/90-user
USER user

# All users can use /home/user as their home directory
ENV HOME=/home/user
RUN chmod 777 /home/user

# Set up the Conda environment
ENV CONDA_AUTO_UPDATE_CONDA=false \
    PATH=/home/user/miniconda/bin:$PATH
COPY pytorch-cuda_environment.yml /app/environment.yml
RUN curl -sLo "$HOME/miniconda.sh" https://repo.continuum.io/miniconda/Miniconda3-py39_4.10.3-Linux-x86_64.sh
RUN echo "1ea2f885b4dbc3098662845560bc64271eb17085387a70c2ba3f29fff6f8d52f  $HOME/miniconda.sh" | sha256sum -c
RUN chmod +x "$HOME/miniconda.sh" \
 && "$HOME/miniconda.sh" -b -p "$HOME/miniconda" \
 && rm "$HOME/miniconda.sh" \
 && conda env update -n base -f /app/environment.yml \
 && rm /app/environment.yml \
 && conda clean -ya

# Set the default command to python3
CMD ["python3"]
